#!/bin/bash

# cd ./auth && docker-compose stop && cd ..
# cd ./server && docker-compose stop -d && cd ..

# cd ./auth && docker-compose up -d && cd ..

# printf "%s" "waiting for Auth Server ..."
# until $(curl --output /dev/null --silent --head --fail http://localhost:8081/health); do
#     printf '.'
#     sleep 5
# done
# printf "\n%s\n"  "Auth Server is online"

# cd ./server && docker-compose up -d && cd ..

function build_auth {
    cd ./auth && docker-compose build && cd ..
}
function build_server {
    cd ./server && docker-compose build && cd ..
}
function build {
    build_auth
    build_server
}
function run_auth {
    cd ./auth && docker-compose up -d && cd ..
}
function run_server {
    printf "%s" "waiting for Auth Server ..."
    until $(curl --output /dev/null --silent --head --fail http://localhost:8081/health); do
        printf '.'
        sleep 5
    done
    printf "\n%s\n"  "Auth Server is online"
    cd ./server && docker-compose up -d && cd ..
}
function run {
    run_auth
    run_server
}
function stop_auth {
    cd ./auth && docker-compose stop && cd ..
}
function stop_server {
    cd ./server && docker-compose stop && cd ..
}
function stop {
    stop_server
    stop_auth
}

case "$1" in
  "--build")
    build
    ;;
  "--build:auth")
    build_auth
    ;;
  "--build:server")
    build_server
    ;;
  "--run")
    run
    ;;
  "--run:auth")
    run_auth
    ;;
  "--run:server")
    run_server
    ;;
  "--stop")
    stop
    ;;
  "--stop:auth")
    stop_auth
    ;;
  "--stop:server")
    stop_server
    ;;
  *)
    echo "You have failed to specify what to do correctly."
    exit 1
    ;;
esac
